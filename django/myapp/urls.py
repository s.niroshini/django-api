from .import views
from django.urls import path

urlpatterns = [
    path('',views.index,name='index'),
    path('context',views.home,name="home"),
    path('view',views.view,name="view")
]