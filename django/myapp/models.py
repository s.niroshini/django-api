from django.db import models

class Lead(models.Model):
    first_name=models.CharField(max_length=50)
    last_name=models.CharField(max_length=100)
    age=models.IntegerField()
    
class Agent(models.Model):
    first_name=models.CharField(max_length=50)
    last_name=models.CharField(max_length=100)
    age=models.IntegerField()
    
